import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import static java.lang.String.*;

public class Baitap01 {
    public static void main(String[] args) {
        bai1();
        System.out.println(bai2());
        System.out.println(bai3());
        bai4();
        bai103();
        bai92();
        bai149();
        bai1StringFormat();
        bai2StringFormat();
        bai3StringFormat();
        bai4StringFormat();
        bai5StringFormat();
        bai6StringFormat();
        bai7StringFormat();
        bai8StringFormat();
        bai9StringFormat();
        bai10StringFormat();

    }

    private static void bai10StringFormat() {
        //10, format LocalDate theo dạng: 30.12.23
        LocalDateTime ldt = LocalDateTime.now();
        System.out.println();
        System.out.printf("%0,2d.%0,2d.%d",ldt.getDayOfMonth(),ldt.getMonthValue(),ldt.get());
    }

    private static void bai9StringFormat() {
//        9, tính căn bậc 2  của 1 số & làm tròn đến số thập phân thứ 3
        double d= Math.sqrt(146);
        System.out.printf("\n %.3f",d);

    }

    private static void bai8StringFormat() {
//        8, format số theo dạng 4 chữ số: 4 -> 0004, 585 -> 0585
        int d=585;
        System.out.println();
        System.out.printf("%0,4d",d);
    }

    private static void bai7StringFormat() {

//        7, format LocalTime theo dạng: 23:55.581
        LocalDateTime ldt  = LocalDateTime.now();
        System.out.println();
        System.out.printf("%0,2d:%0,2d.%0,2d",ldt.getHour(),ldt.getMinute(),ldt.getSecond());
    }


    private static void bai6StringFormat() {
        //6, format số 5.8548 theo dạng làm tròn 5.855
        double d = 5.8548;
        System.out.println();
        System.out.printf("%.3f",d);
    }

    private static void bai5StringFormat() {
        //format 1 số long theo dạng: 1,000,000,855
        System.out.println();
        long l = 1000000855;
        System.out.printf("%,d",l);

    }

    private static void bai4StringFormat() {
        //4, format LocalDate theo dạng: 200/2001
        LocalDateTime ldt  = LocalDateTime.now();
        System.out.println();
        System.out.printf("%d/%d",ldt.getDayOfYear(),ldt.getYear());
    }

    private static void bai3StringFormat() {
        //3, format số double theo dạng: 15.525,00
        double d=15525;
        System.out.println();
        System.out.printf(Locale.GERMAN,"%,.2f",d);

    }

    private static void bai2StringFormat() {
//        2, format LocalDateTime theo dạng: 13/01/2002 01:02:20

        LocalDateTime ldt = LocalDateTime.now();
        System.out.printf("\n%0,2d/%0,2d/%d %0,2d:%0,2d:%0,2d",ldt.getDayOfMonth(),ldt.getMonthValue(),ldt.getYear()
                                            ,ldt.getHour(),ldt.getMinute(),ldt.getSecond()

        );

    }

    private static void bai1StringFormat() {
//        1, format LocalDate theo dạng 12/31/2001

        LocalDateTime ldt = LocalDateTime.now();

//        System.out.println("%d",8);
        System.out.printf("%3$d/%2$0,2d/%1$0,2d",ldt.getDayOfMonth(),ldt.getMonthValue(),ldt.getYear());

    }


    private static void bai1() {
        char[] array  ={'H','E','L','L','O'};
        StringBuilder sb = new StringBuilder();
        System.out.println(sb.append(array));
    }
    private static long bai2() {
        long t1= System.currentTimeMillis();
        StringBuilder sb  = new StringBuilder(50000);
        for (int i=0;i<5000;i++){
            Random rd = new Random();
            char rdChar = (char) (rd.nextInt(26)+'a');
            sb.append(rdChar);
        }
        long t2= System.currentTimeMillis();
        return t2-t1;
    }
    private static long bai3() {
        long t1 = System.currentTimeMillis();
        String s1="";
        for (int i=0;i<50000;i++){
            Random rd = new Random();
            char rdChar = (char) (rd.nextInt(26)+'a');
            s1+=rdChar;
        }
        long t2 = System.currentTimeMillis();
        return t2-t1;
    }
    private static void bai4() {
        bai2();
        bai3();
        System.out.println((float) bai3()/bai2());
    }
    private static void bai105() {
        String s1 = "aa";
        String s2 = "abca acaa";
        int dem=0;
        String[] ss= new String[s2.length()-1];
        for (int i=0 ; i < s2.length()-1;i++){
            for (int j =0; j < s2.length();j++){
                ss[i] = s2.substring(j,j+2);
            }
        }
        for (int i=0;i<ss.length;i++){
            System.out.print(ss[i]+" ");
            if (ss[i].equals(s1)){
                dem++;
            }
        }
        System.out.println(dem);
        System.out.println(s2.length());
//        int dem = 0 ;


        System.out.println(dem);
    }

    private static void bai103() {
        String s1= "10002.1452D";
//        String s2= "45454135215646456";
        String s3="25.135F";
        String s4= "21.6464D";
        if (s1.charAt(s1.length()-1)=='F'){
            System.out.println( format(s1,"%f"));
        }
        if (s1.charAt(s1.length()-1)=='D'){
            String s2 = s1.substring(0,s1.length()-1);
            System.out.println( format(s2,"%f"));
        }
        if (s1.charAt(s1.length()-1)<='9' & s1.charAt(s1.length()-1)>='0'){
            System.out.println( format(s1,"%d"));
        }
    }

    private static void bai92() {
        String s1= "This is the test string";
        String s2="st";
        String s3= s1.replace("st","");
        System.out.println(s3);
    }

    private static void bai149() {
        String s1= "155";
        int l1= s1.length();
        char[] c = new char[10-l1];
        for (int i=0 ;i<c.length;i++){
            s1='0'+s1;

        }
        System.out.println(s1);
    }
}
